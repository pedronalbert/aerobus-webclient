/** Dependencies */
import React from 'react';

/** Components */

class StationsTableRow extends React.Component {
  render() {
    const {station} = this.props;

    return <tr>
      <td>{station.id}</td>
      <td className="text-capitalize">{station.name}</td>
      <td className="text-capitalize">{station.state}</td>
      <td>
        <i 
          onClick={this._handleClickDeleteAction.bind(this)}
          className="fa fa-trash-o action-icon"
        />
      </td> 
    </tr>
  }

  _handleClickDeleteAction() {
    if(this.props.onClickDeleteAction) {
      this.props.onClickDeleteAction(this.props.station.id);
    }
  }
}

StationsTableRow.propTypes = {
  station: React.PropTypes.object.isRequired,
  onClickDeleteAction: React.PropTypes.func
};

export default StationsTableRow;