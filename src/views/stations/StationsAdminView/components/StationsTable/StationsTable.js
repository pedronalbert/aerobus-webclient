/** Dependencies */
import React from 'react';
import {Alert} from 'react-bootstrap';

/** Components */
import StationsTableRow from './StationsTableRow';

class StationsTable extends React.Component {
  render() {
    const {stations} = this.props;

    if(stations.length > 0) {
      return <div>
        <table className="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>

          <tbody>
            {stations.map(station => {
              return <StationsTableRow 
                key={station.id} 
                station={station} 
                onClickDeleteAction={this._handleClickDeleteAction.bind(this)}
              />
            })}
          </tbody>
        </table>
      </div>
    } else {
      return <div>
        <Alert bsStyle="warning">
          No hay estaciones registradas
        </Alert>
      </div>
    }
  }

  _handleClickDeleteAction(stationId) {
    if(this.props.onClickDeleteAction) {
      this.props.onClickDeleteAction(stationId);
    }
  }
}

StationsTable.propTypes = {
  stations: React.PropTypes.array.isRequired,
  onClickDeleteAction: React.PropTypes.func
};

export default StationsTable;