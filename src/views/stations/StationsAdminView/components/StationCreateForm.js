/** Dependencies */
import React from 'react';
import {Input, ButtonInput} from 'react-bootstrap';
import {reduxForm} from 'redux-form';
import ClassNames from 'classnames';
 
/** Components */
import {StationsAPI} from 'utils';

class StationCreateForm extends React.Component {
  render() {
    const styles = this._getStyles();
    const {fields: {name, state}, handleSubmit, errors, submitting} = this.props;

    return <div>
      <form onSubmit={handleSubmit}>
        <Input 
          bsStyle={name.touched && errors.name ? 'error' : null}
          type="text" 
          label="Nombre" 
          placeholder="Terminal"
          help={name.touched && errors.name}
          {...name}
        />

        <Input 
          bsStyle={state.touched && errors.state ? 'error' : null}
          type="text" 
          label="Estado" 
          placeholder="Tachira" 
          help={state.touched && errors.state}
          {...state}
        />

        <ButtonInput 
          style={styles.buttonSubmit} 
          bsStyle="primary" 
          type="submit" 
          value="REGISTRAR"
          disabled={submitting}
        />
      </form>
    </div>
  }

  _getStyles() {
    let styles = {
      buttonSubmit: {
        width: '100%',
        marginTop: '1em'
      }
    };

    return styles;
  }
}

function validate(values) {
  let errors = {};

  if(!values.name) {
    errors.name = 'El nombre es obligatorio';
  }

  if(!values.state) {
    errors.state = 'El estado es obligatorio';
  }

  return errors;
}

StationCreateForm = reduxForm({
  form: 'station_create',
  fields: ['name', 'state'],
  validate
})(StationCreateForm);

export default StationCreateForm;