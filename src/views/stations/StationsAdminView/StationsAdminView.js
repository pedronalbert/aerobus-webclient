/** Dependencies */
import React from 'react';
import {connect} from 'react-redux';
import {Col, Grid} from 'react-bootstrap';
import Alertify from 'alertifyjs';
import {reset as resetForm} from 'redux-form';

/** Components */
import StationCreateForm from './components/StationCreateForm';
import PaperCard from 'components/PaperCard';
import StationsActions from 'actions/StationsActions';
import StationsTable from './components/StationsTable';
import {ValidationException} from 'utils/Exceptions';
import {StationsAPI} from 'utils';

class StationsAdminView extends React.Component {
  render() {
    const {stations} = this.props;

    return <div>
      <Grid fluid>
        <Col md={8}>
          <PaperCard title="Lista de estaciones">
            <StationsTable 
              stations={stations}
              onClickDeleteAction={this._handleClickDeleteAction.bind(this)}
            />
          </PaperCard>
        </Col>

        <Col md={4}>
          <PaperCard title="Registrar Estacion">
            <StationCreateForm
              onSubmit={this._handleSubmitCreate.bind(this)}
            />
          </PaperCard>
        </Col>

        <Col md={12}>
          <button onClick={this._handleClick.bind(this)}>Cargar todos</button>
        </Col>
      </Grid>
    </div>
  }

  _handleClick() {
    this.props.dispatch(StationsActions.getAll());
  }

  _handleSubmitCreate(station) {
    return new Promise((resolve, reject) => {
      StationsAPI
        .create(station)
        .then(newStation => {
          this.props.dispatch(StationsActions.insert(newStation));
          this.props.dispatch(resetForm('station_create'));
          Alertify.notify('Estacion registrada', 'success', 5);

          return resolve();
        })
        .catch(ValidationException, exception => {
          Alertify.notify(exception.message, 'error', 5);

          return reject(exception.reduxFields);
        })
    })
  }

  _handleClickDeleteAction(stationId) {
    const message = `¿Esta seguro que desea eliminar la estacion #${stationId}?`;

    Alertify.confirm(message, () => {
      StationsAPI.destroy(stationId);
      this.props.dispatch(StationsActions.destroy(stationId));
      Alertify.notify(`Estacion #${stationId} ha sido eliminada`, 'success', 5);
    })
  }
}

function mapStateToProps(state) {
  let stations = [];
  let stationsMap = state.entities.get('stations').toArray();

  _.forEach(stationsMap, (stationMap) => {
    stations.push(stationMap.toObject());
  });

  return {
    stations
  };
}

StationsAdminView = connect(mapStateToProps)(StationsAdminView);

export default StationsAdminView;