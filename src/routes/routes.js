import React                 from 'react';
import { Route, IndexRoute } from 'react-router';
import CoreLayout            from 'layouts/CoreLayout';

/** Vies */
import StationsAdminView from 'views/stations/StationsAdminView';

export default (
  <Route        component={CoreLayout} path='/'>
    <IndexRoute component={StationsAdminView} />
  </Route>
);
