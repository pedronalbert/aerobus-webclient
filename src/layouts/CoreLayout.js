/** Dependencies */
import React from 'react';

/** Css Dependencies */
import alertifyCss from 'alertifyjs/build/css/alertify.css';
import coreCss from 'styles/core.scss';

/** Components */

class CoreLayout extends React.Component {
  static propTypes = {
    children : React.PropTypes.element
  }

  render () {
    const styles = this._getStyles();

    return <div>
      <nav style={styles.leftSidebar}>
      </nav>
      <div style={styles.content}>
        {this.props.children}
      </div>
    </div>
  }

  _getStyles() {
    const sidebarWidth = '15.75em';

    let styles = {
      leftSidebar: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: sidebarWidth,
        height: '100vh',
        backgroundColor: 'red',
      },

      content: {
        marginLeft: sidebarWidth,
        padding: '2em'
      }
    };

    return styles;
  }
}


export default CoreLayout;