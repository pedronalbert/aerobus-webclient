/** Dependencies */
import $ from 'jquery';
import {normalize, arrayOf} from 'normalizr';
import Promise from 'bluebird';
import Alertify from 'alertifyjs';

/** others */
import {StationSchema} from './NormalizrSchemas';
import {ValidationException} from 'utils/Exceptions';

let StationsAPI = {
  url: 'http://localhost/stations',

  getAll() {
    return new Promise((resolve, reject) => {
      $.get(this.url)
        .done(response => {
          resolve(response);
        })
        .fail(err => {
          reject(err);
        })
    });
  },

  create(newStationData) {
    return new Promise((resolve, reject) => {
      $.post(this.url, newStationData)
        .done(response => {
          return resolve(response);
        })
        .fail((err) => {
          const {status, responseJSON: response} = err;

          if(status === 400) {
            return reject(new ValidationException(
              response.fields,
              response.message
            ));
          }

          reject(err);
        })
    })
  },

  destroy(stationId) {
    let data = {
      _method: 'delete'
    };

    return new Promise((resolve, reject) => {
      $.post(`${this.url}/${stationId}`, data)
        .done(response => {
          return resolve(response);
        })
        .fail(err => {
          return reject(err);
        })
    })
  }
};

export default StationsAPI;