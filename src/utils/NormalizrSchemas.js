import {Schema} from 'normalizr';

const StationSchema = new Schema('stations');

export {
  StationSchema
}