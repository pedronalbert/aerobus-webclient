import _ from 'lodash';

function ValidationException(fields = [], message = '') {
  this.name = "ValidationException";
  this.message = message;
  this.fields = fields;
  this.reduxFields = {};

  _.forEach(this.fields, (field) => {
    let fieldName = _.keys(field)[0];
    let fieldMessage = field[fieldName];

    this.reduxFields[fieldName] = fieldMessage;
  })
}

ValidationException.prototype = Object.create(Error.prototype);
ValidationException.prototype.constructor = ValidationException;

export default {
  ValidationException
};