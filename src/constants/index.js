import _ from 'lodash';
import StationsConstants from './StationsConstants';

let constants = {};

_.merge(constants, StationsConstants);


export default constants;