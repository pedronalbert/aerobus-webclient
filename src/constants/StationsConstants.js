import { createConstants } from '../utils';

export default createConstants(
  'STATIONS_GET_ALL',
  'STATION_INSERT',
  'STATION_DESTROY'
);
