/** Dependencies */
import React from 'react';

class PaperCard extends React.Component {
  render() {
    const styles = this._getStyles();
    const {title} = this.props;
    let paperTitleNode;

    if(title) {
      paperTitleNode = <div style={styles.header}>
        <span style={styles.title}>{title}</span>
      </div>
    }

    return <div style={styles.root} >
      {paperTitleNode}

      <div style={styles.content}>
        {this.props.children}
      </div>
    </div>;
  }

  _getStyles() {
    let styles = {
      root: {
        boxShadow: '0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24)'
      },

      header: {
        display: 'flex',
        alignItems: 'center',
        height: '3.4em',
        padding: '0em 1em'
      },

      title: {
        fontSize: '1.14em'
      },

      content: {
        padding: '1em 1.43em 1.43em 1.43em'
      }
    };

    return styles;
  }
};

PaperCard.propTypes = {
  title: React.PropTypes.string
}

export default PaperCard;