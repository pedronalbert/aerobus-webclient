/** Dependencies */
import Immutable from 'immutable';
import {resolve, reject} from 'redux-simple-promise';

import {
  STATIONS_GET_ALL,
  STATION_INSERT,
  STATION_DESTROY} from 'constants';

const initialState = Immutable.fromJS({
  stations: {}
});

function entities(state = initialState, action) {
  switch(action.type) {
    case STATION_INSERT:
    case resolve(STATIONS_GET_ALL):
      let newEntities = Immutable.fromJS(action.payload.entities);

      return state.mergeDeep(newEntities);
    case STATION_DESTROY:
      let id = action.payload.id.toString();

      return state.removeIn(['stations', id]);
  }

  return state;
}

export default entities;