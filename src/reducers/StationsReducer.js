/** Dependencies */
import Immutable from 'immutable';
import {resolve, reject} from 'redux-simple-promise';

import {
  STATIONS_GET_ALL,
  STATION_CREATE} from 'constants';

const initialState = Immutable.fromJS({
  ids: []
});

function stations(state = initialState, action) {

  return state;
}

export default stations;