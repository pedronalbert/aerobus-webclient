import { combineReducers }    from 'redux';
import { routeReducer }       from 'redux-simple-router';
import {reducer as formReducer} from 'redux-form';
import StationsReducer from './StationsReducer';
import EntitiesReducer from './EntitiesReducer';


export default combineReducers({
  entities: EntitiesReducer,
  stations: StationsReducer,
  routing: routeReducer,
  form: formReducer
});
