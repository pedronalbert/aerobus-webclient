/** Dependencies */
import {normalize, arrayOf} from 'normalizr';

import {StationSchema} from 'utils/NormalizrSchemas';

import {
  STATIONS_GET_ALL,
  STATION_INSERT,
  STATION_DESTROY} from 'constants';
import {StationsAPI} from 'utils';

function getAll() {

  let getAllPromise = new Promise((resolve, reject) => {
    StationsAPI
      .getAll()
      .then(stations => {
        stations = normalize({stations}, {
          stations: arrayOf(StationSchema)
        });

        resolve(stations);
      })
  });


  return {
    type: STATIONS_GET_ALL,
    payload: {
      promise: getAllPromise
    }
  }
}

function insert(newStation) {
  newStation = normalize({station: newStation}, {
    station: StationSchema
  });

  return {
    type: STATION_INSERT,
    payload: {
      ...newStation
    }
  };
}

function destroy(id) {
  return {
    type: STATION_DESTROY,
    payload: {
      id
    }
  };
}

export default {
  getAll,
  insert,
  destroy
};